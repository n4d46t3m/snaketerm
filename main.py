#!/usr/bin/python3

'''

SnakeTerm - A snake game written in Python and Curses

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

CopyLeft 2022

'''

from Snake import Snake
from InitScreen import InitScreen

if __name__ == '__main__':
    
    snakeObj = Snake()
    
    try:
        Screen = InitScreen(snakeObj.snakeAutocollisionDead)
        snakeObj.initSnake(Screen.centerCoordY,Screen.centerCoordX)
        snakeObj.generateRandCoin(Screen.Y,Screen.X)
        while True:
            snakeObj.snakeDirection = Screen.getKeyPressed(snakeObj.gameOver,snakeObj.gameInit)
            if snakeObj.snakeDirection == 'pauseToggle' and not snakeObj.gameOver:
                snakeObj.pause = False if snakeObj.pause else True
            elif snakeObj.snakeDirection == 'pauseOn':
                snakeObj.pause = True
            elif snakeObj.snakeDirection == 'pauseOff':
                snakeObj.pause = False
            elif snakeObj.snakeDirection == 'toggleAutocollisions':
                    snakeObj.snakeAutocollisionDead = Screen.snakeAutocollisions
            if not snakeObj.pause:
                if snakeObj.snakeDirection == 'newGame':
                    snakeObj.gameInit = False
                    snakeObj.gameOver = False
                    snakeObj.snakeDirection = snakeObj.snakeDefaultDirection
                    snakeObj.initSnake(S.centerCoordY,S.centerCoordX)
                    snakeObj.generateRandCoin(Screen.Y,Screen.X)
                #elif snakeObj.snakeDirection == 'toggleAutocollisions':
                #    snakeObj.snakeAutocollisionDead = Screen.snakeAutocollisions
                elif snakeObj.snakeDirection == 'errorSize':
                    continue
                snakeObj.moveSnake()
                if snakeObj.snakeEatingCoin(Screen.Y,Screen.X) and Screen.enableColorTrill:
                    Screen.colorTrill()
                snakeObj.checkIfSnakeIsDied(Screen.Y,Screen.X)
            #Screen.setGameData(snakeObj.snake,snakeObj.coin,snakeObj.score,snakeObj.gameOver,snakeObj.gameInit) # WIP !!! TODO !!!
            Screen.refresh(snakeObj.snake,snakeObj.coin,snakeObj.score,snakeObj.gameOver,snakeObj.gameInit)
    except KeyboardInterrupt:
        S.exit()
        
