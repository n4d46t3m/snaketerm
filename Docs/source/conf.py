# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
from os import path as osPath
from sys import path as sysPath
sysPath.insert(0, osPath.abspath('../..'))


# -- Project information -----------------------------------------------------

project = 'SnakeTerm'
copyright = '2022, netcat'
author = 'netcat'

# The full version, including alpha/beta/rc tags
release = '1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
            'sphinx.ext.autodoc',
            'sphinx_mdinclude',
            'sphinx.ext.todo',
            'sphinx.ext.imgmath'
            ]
            
# Setting some defaults for the autodoc
autodoc_default_options = {
                            'members':True,
                            'member-order':'bysource',
                            'undoc-members':True,
                            'private-members':True,
                            'special-members':'__init__,__str__,__getitem__,__setitem__'
                        }

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The suffix(es) of source filenames.
# You can specify multiple suffix as a list of string:
#
source_suffix = ['.rst', '.md']

# The master toctree document.
master_doc = 'index'

# General information about the project.
project = 'SnakeTerm'
copyright = '2022, netcat n4d4s'
author = 'netcat n4d4s'

# The version info for the project you're documenting, acts as replacement for
# |version| and |release|, also used in various other places throughout the
# built documents.
#
# The short X.Y version.
version = '1'
# The full version, including alpha/beta/rc tags.
release = '1.0.0'

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
language = 'en'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'classic'

# Setting a favicon
html_favicon = 'icon16x16.png'

# Theme options are theme-specific and customize the look and feel of a theme
# further.  For a list of options available for each theme, see the
# documentation.
#
html_theme_options = {
                        'stickysidebar':True,
                        'bodyfont':'courier new',
                        'bgcolor':'#000000',
                        'textcolor':'#EBEBEB',
                        'linkcolor':'#1ECE00',
                        'visitedlinkcolor':'#00AC01',
                        'codetextcolor':'#64FF8B',
                        'codebgcolor':'#282828',
                        'sidebarbgcolor':'#1F1F1F',
                        'sidebartextcolor':'#00A941',
                        'sidebarlinkcolor':'#00FF40',
                        'footerbgcolor':'#000000',
                        'footertextcolor':'#00FF40',
                        'relbarbgcolor':'#000000',
                        'relbartextcolor':'#00A941',
                        'relbarlinkcolor':'#00FF40',
                        'headbgcolor':'#5B635E',
                        'headtextcolor':'#C1FFC3'
                    }

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_css_files = [
    'css/classicCustom.css',
]

# Custom sidebar templates, must be a dictionary that maps document names
# to template names.
#
# This is required for the alabaster theme
# refs: http://alabaster.readthedocs.io/en/latest/installation.html#sidebars
html_sidebars = {
    '**': [
        'relations.html',  # needs 'show_related': True theme option to display
        'searchbox.html',
    ]
}