from random import randint

class Snake():
    
    def __init__(self):
        '''__init__
        Init Snake() class
        '''
        self.snake = [] # It will contain Snake body coordinates
        self.snakeDefaultDirection = 'right' # The direction that must have Snake when the game starts
        # Following var self.snakeDirection in addition to containing the 
        # direction that must take the Snake may also contain directives 
        # generated by particular situations or by pressing non directional keys 
        self.snakeDirection = self.snakeDefaultDirection
        # self.snakeCurrentDirection can contain only direction directives: 'left' 'right' 'up' 'down'
        self.snakeCurrentDirection = self.snakeDirection
        self.snakeAutocollisionDead = True # True if Snake must die if collide with himself
        self.coin = [] # It will contain coin coordinates
        self.coinBody = 'O' # The char that represent the item that Snake must eat
        self.score = 0 # Setting the score to 0
        self.gameInit = True # This is equal to True until the player start the first game
        self.gameOver = True # This is equal to True when the player has lost or haven't yet started the game
        self.pause = False # This is equal to True when the game is paused
        
    def initSnake(self,Y=1,X=1):
        '''initSnake
        
        Reset the score and generate the first part of Snake (the head) 
        at given coordinates resetting ``self.snake`` 
        
        :param Y: Snake head Y coordinate
        :type Y: int
        :param X: Snake head X coordinate
        :type X: int
        
        :return: 
        :rtype: void
        '''
        self.score = 0
        self.startPositionY = Y
        self.startPositionX = X
        self.snakeBody = 'X'
        self.snake = [[self.startPositionY, self.startPositionX, self.snakeBody]]
        
    def moveSnake(self):
        '''moveSnake
        
        Moves Snake according to the current direction ``self.snakeDirection``
        
        This set ``self.snakeCurrentDirection`` so when the player doesn't 
        press any key (``self.snakeDirection == None``) the Snake will 
        keep moving while maintaining direction.
        
        To move Snake are updated ``self.startPositionX`` and ``self.startPositionY`` 
        that are the coordinates of the Snake head and ``self.snake`` that 
        is the list that contain the coordinates of all Snake parts

        :return: 
        :rtype: void
        '''
        if self.snakeDirection == None:
            self.snakeDirection = self.snakeCurrentDirection
        if self.snakeDirection == 'left':
            self.startPositionY = self.startPositionY
            self.startPositionX = self.startPositionX - 1
            self.snakeCurrentDirection = self.snakeDirection
        elif self.snakeDirection == 'right':
            self.startPositionY = self.startPositionY
            self.startPositionX = self.startPositionX + 1
            self.snakeCurrentDirection = self.snakeDirection
        elif self.snakeDirection == 'up':
            self.startPositionY = self.startPositionY - 1
            self.startPositionX = self.startPositionX
            self.snakeCurrentDirection = self.snakeDirection
        elif self.snakeDirection == 'down':
            self.startPositionY = self.startPositionY + 1
            self.startPositionX = self.startPositionX
            self.snakeCurrentDirection = self.snakeDirection
        else:
            self.snakeDirection = self.snakeCurrentDirection
        
        snakeLen = len(self.snake)-1
        for snakePart in range(snakeLen,0,-1):
            self.snake[snakePart][0] = self.snake[snakePart-1][0]
            self.snake[snakePart][1] = self.snake[snakePart-1][1]
        self.snake[0][0] = self.startPositionY
        self.snake[0][1] = self.startPositionX
            
    def generateRandCoin(self,tableY,tableX):
        '''generateRandCoin
        
        Generate the item that Snake must eat to groove and obtain points.
        
        There is an ugly recursion and when Snake will reach big dimensions 
        slowdowns may occur if the item to eat appear on the snake body
        
        :param tableY: Width of the game area, used to generate coins only inside the game area
        :type tableY: int
        :param tableX: Height of the game area, used to generate coins only inside the game area
        :type tableX: int
        
        :return: 
        :rtype: void
        '''
        self.coinCoordY = randint(1, tableY-3)
        self.coinCoordX = randint(1, tableX-3)
        for snakePart in self.snake:
            if snakePart[0] == self.coinCoordY and snakePart[1] == self.coinCoordX:
                self.generateRandCoin(tableY,tableX)
        self.coin = [self.coinCoordY,self.coinCoordX,self.coinBody]
        
    def snakeEatingCoin(self,tableY,tableX):
        '''snakeEatingCoin
        
        If Snake eat a coin the score is increased and is called 
        ``self.generateRandCoin`` to generate another coin
        
        :param tableY: Width of the game area, passed to ``self.generateRandCoin``
        :type tableY: int
        :param tableX: Height of the game area, passwd to ``self.generateRandCoin``
        :type tableX: int
        
        :return: True if Snake eat the coin, otherwise False
        :rtype: boolean
        '''
        if self.coin[0] == self.startPositionY and self.coin[1] == self.startPositionX:
            self.snake.append([self.snake[len(self.snake)-1][0],self.snake[len(self.snake)-1][1],self.snakeBody])
            self.score = self.score + 10
            self.generateRandCoin(tableY,tableX)
            return True
        return False
            
    def checkIfSnakeIsDied(self,tableY,tableX):
        '''
        checkIfSnakeIsDied
        
        This method will set ``self.gameOver = True`` if Snake collide with borders or with himself 
        
        :param tableY: Width of the game area, used to check if Snake collide with borders
        :type tableY: int
        :param tableX: Height of the game area, used to check if Snake collide with borders
        :type tableX: int
        
        :return: True if Snake is died, otherwise False
        :rtype: boolean
        '''
        if self.startPositionX == 0 or self.startPositionX == tableX-2:
            self.gameOver = True
            return True
        elif self.startPositionY == 0 or self.startPositionY == tableY-2:
            self.gameOver = True
            return True
        snakeLen = len(self.snake)-1
        if self.snakeAutocollisionDead:
            for snakePart in range(snakeLen,1,-1):
                if self.startPositionY == self.snake[snakePart][0] and self.startPositionX == self.snake[snakePart][1]:
                    self.gameOver = True
                    return True
        return False
