import curses
# import psutil # Not used now but useful when developing
from datetime import datetime

class InitScreen():
    
    def __init__(self,snakeAutocollisions):
        '''__init__
        
        Create curses window and the sub window that will contain data 
        to show and update
        
        :param snakeAutocollisions: True if Snake must die if collide with himself
        :type snakeAutocollisions: boolean
        '''
        self.stdscr = curses.initscr()
        #self.stdscr.clrtoeol()
        curses.start_color()
        curses.use_default_colors()
        curses.cbreak()
        curses.noecho()
        self.stdscr.nodelay(True)
        self.stdscr.keypad(1)
        curses.curs_set(0)
        self.YXPairs = [[20,70],[20,100],[20,130],[20,160],[24,70],[24,100],[24,130],[24,160],[30,70],[30,100],[30,130],[30,160]]
        self.currentYXPair = 0
        self.Y = self.YXPairs[self.currentYXPair][0]
        self.X = self.YXPairs[self.currentYXPair][1]
        self.defaultColorPair = 0 # 0 for system colors
        self.gameOverChoice = 0 # 0 for new game / 1 for the options / 2 to exit
        self.optionChoice = 0 # 0 to change table dimensions / 1 to change speed / 2 change color / 3 disable autocollisions / 4 disable color trill
        # self.options così ha poco senso, dovrebbe essere un dizionario o una lista di liste del tipo [[opzione1,valoreOpzione1],[opzione2,valoreOpzione2]]
        # Praticamente ora self.options serve solo per capire quante opzioni ci sono senza hardcodare numeri
        self.options = ['Interface dimensions (X/Y)','Interface refresh time','Current color','Snake autocollisions','Color trill']
        self.errorSize = False # Diventa True se le dimensioni del terminale sono inferiori rispetto a quelle dell'interfaccia 
        
        for i in range(16): # 16 is black
            curses.init_pair(i,i,-1)

        self.enableColorTrill = True
        self.snakeAutocollisions = snakeAutocollisions

        self._initSubWin()
        self.currentPage = 'home'
        self._minRefreshTime = 40
        self._maxRefreshTime = 400
        self._refreshStep = 10
        self._setRefreshTime() # Refresh time in MS
        
    def _setInterfaceDimensions(self,Y=20,X=70):
        '''_setInterfaceDimensions
        
        Set interface dimensions setting ``self.X`` and ``self.y``
        
        :param Y: Interface width
        :type Y: int
        :param X: Interface height
        :type X: int
        
        :return: 
        :rtype: void
        '''
        self.Y = Y
        self.X = X
        
    def _setRefreshTime(self,refreshTime=100):
        '''_setRefreshTime
        
        Given a value set ``self.refreshTime``
        
        :param refreshTime: Refresh time in milliseconds
        :type refreshTime: int
        
        :return: 
        :rtype: void
        '''
        self.refreshTime = refreshTime
        
    def _initSubWin(self):
        '''_initSubWin
        
        Write the sub window
        
        :return: 
        :rtype: void
        '''
        #self.Y, self.X = self.stdscr.getmaxyx() # I know.. curses / ncurses think with Y/X and not with X/Y # This is for max terminal dimensions
        self.centerCoordY = int((self.Y/2)-1)
        self.centerCoordX = int((self.X/2)-1)
        self.bar = self.stdscr.derwin(self.Y-1, self.X-1, 0, 0)
        self.bar.bkgd(' ',curses.color_pair(self.defaultColorPair))
        self.bar.box()
        self._printFooter()

    def _printFooter(self):
        '''_printFooter
        
        Print the footer
        
        :return: 
        :rtype: void
        '''
        self.bar.addstr(self.Y-2, 1,' 00:00:00 ',curses.A_BOLD|curses.A_REVERSE)
        self.bar.addstr(self.Y-2, 10,' SCORE 00000000 ',curses.A_BOLD|curses.A_REVERSE)
        '''
        self.bar.addstr(self.Y-2, 10,' CPU: '+str('0.00% '),curses.A_BOLD|curses.A_REVERSE)
        self.bar.addstr(self.Y-2, 21,' RAM: '+str('0.00% '),curses.A_BOLD|curses.A_REVERSE)
        self.bar.addstr(self.Y-2, 32,' Press \'o\' to see option page ',curses.A_BOLD|curses.A_REVERSE)
        '''
        
    def _printOptionPage(self):
        '''_printOptionPage
        
        Print the option page. 
        
        Vertical line overflow is managed by bad / ugly code but I will 
        write some better code to implement text scrolling
        
        :return: 
        :rtype: void
        '''
        lastPageLine = self.Y-4
        #self.bar.addstr(self.Y-2, 32,' Press \'o\' to back the game ',curses.A_BOLD|curses.A_REVERSE) # Overwrite footer content
        self.bar.addstr(1,1,' ')
        self.bar.addstr(2,1,' Classic Snake game with Python/Curses',curses.A_BOLD)
        self.bar.addstr(3,1,' ')
        if(lastPageLine > 4):# Don't print this line if overflow the last page line
            self.bar.addstr(4,1,' Here are some useful keys that you can use during the game:')
        #if(lastPageLine > 4):# Don't print this line if overflow the last page line
        #    self.bar.addstr(4,1,' ')
        if(lastPageLine > 5):# Don't print this line if overflow the last page line
            self.bar.addstr(5,1,'     c to change the interface color')
        if(lastPageLine > 6):# Don't print this line if overflow the last page line
            self.bar.addstr(6,1,'     o to toggle this option page')
        if(lastPageLine > 7):# Don't print this line if overflow the last page line
            self.bar.addstr(7,1,'     p to pause game')
        if(lastPageLine > 8):# Don't print this line if overflow the last page line
            self.bar.addstr(8,1,'     q or ESC to quit the software')
        if(lastPageLine > 10):# Don't print this line if overflow the last page line
            self.bar.move(10,1)
            self.bar.clrtoeol()
            self.bar.addstr(10,1,' Interface dimensions (X/Y) (Can\'t change during game): '+str(self.X)+'/'+str(self.Y)+' ',curses.A_BOLD|curses.A_REVERSE if self.optionChoice == 0 else curses.A_BOLD)
        if(lastPageLine > 11):# Don't print this line if overflow the last page line
            self.bar.move(11,1)
            self.bar.clrtoeol()
            self.bar.addstr(11,1,' Interface refresh time (Can\'t change during game): '+str(self.refreshTime)+'ms ',curses.A_BOLD|curses.A_REVERSE if self.optionChoice == 1 else curses.A_BOLD)
        if(lastPageLine > 12):# Don't print this line if overflow the last page line
            self.bar.move(12,1)
            self.bar.clrtoeol()
            self.bar.addstr(12,1,' Current color: '+('DEFAULT ' if self.defaultColorPair == 0 else str(self.defaultColorPair)+' '),curses.A_BOLD|curses.A_REVERSE if self.optionChoice == 2 else curses.A_BOLD)
        if(lastPageLine > 13):# Don't print this line if overflow the last page line
            self.bar.move(13,1)
            self.bar.clrtoeol()
            self.bar.addstr(13,1,' Snake autocollisions (Can\'t change during game): '+('ON ' if self.snakeAutocollisions else 'OFF '),curses.A_BOLD|curses.A_REVERSE if self.optionChoice == 3 else curses.A_BOLD)
        if(lastPageLine > 14):# Don't print this line if overflow the last page line
            self.bar.move(14,1)
            self.bar.clrtoeol()
            self.bar.addstr(14,1,' Color trill: '+('ON ' if self.enableColorTrill else 'OFF '),curses.A_BOLD|curses.A_REVERSE if self.optionChoice == 4 else curses.A_BOLD)
        self.bar.box()
        self.bar.addstr(0,1,' SNAKE TERM OPTIONS ',curses.A_BOLD|curses.A_REVERSE)
        self.bar.addstr(lastPageLine,1,' CopyLeft 2022 n4d46t3m netcat',curses.A_BOLD) # Fixed one line above the footer
        
    def _printHomePage(self, snake=[[1,1,'-']], coin=[2,2,'O']):
        '''_printHomePage
        
        Given the data write the game screen.
         
        This mean that the screen is cleaned, then is drawed the coin and 
        Snake. Finally are drawed borders and game header
        
        .. todo::
            Modify the method name because ``_printHomePage`` creates confusion
        
        :param snake: Multidimensional list that contain the coordinates of all snake body parts and the char that will represent it
        :type snake: list
        :param coin: Current coordinates and char that will represent the coin
        :type coin: list
        
        :return: 
        :rtype: void
        '''
        self.bar.clear()
        self.bar.addch(coin[0],coin[1],coin[2],curses.A_BOLD)
        for snakeBodyPart in snake:
            self.bar.addch(snakeBodyPart[0],snakeBodyPart[1],snakeBodyPart[2])
        self.bar.box()
        self.bar.addstr(0,1,' SNAKE'+(' '*(self.YXPairs[self.currentYXPair][1]-9)),curses.A_BOLD|curses.A_REVERSE)#Header
        
    def _printGameOverPage(self,gameInit):
        '''_printGameOverPage
        
        Render the welcome screen when the . 
        
        The screen is displayed until the player start the game, so when
        there is a gameover status or the player haven't started the first game
        
        :param gameInit: True until the player started the first game
        :type gameInit: boolean
        
        :return: 
        :rtype: void
        '''
        if gameInit:
            self.bar.addstr(0,1,' SNAKE'+(' '*(self.YXPairs[self.currentYXPair][1]-9)),curses.A_BOLD|curses.A_REVERSE)
            self.bar.addstr(2,1,' XXXXX  X   X  XXXXX  X  XX  XXXXX    XXXXX  XXXXX  XXXXX  XX XX',curses.A_BOLD)
            self.bar.addstr(3,1,' X      XX  X  X   X  X X    X          X    X      X   X  X X X',curses.A_BOLD)
            self.bar.addstr(4,1,' XXXXX  X X X  XXXXX  XXXX   XXX        X    XXX    XXXXX  X   X',curses.A_BOLD)
            self.bar.addstr(5,1,'     X  X  XX  X   X  X   X  X          X    X      X XX   X   X',curses.A_BOLD)
            self.bar.addstr(6,1,' XXXXX  X  XX  X   X  X   X  XXXXX      X    XXXXX  X   X  X   X',curses.A_BOLD)
            self.bar.addstr(8,3,' Start Game ',curses.A_BOLD|curses.A_REVERSE if self.gameOverChoice == 0 else curses.A_BOLD)
        else:
            self.bar.addstr(0,1,' SNAKE - Game Over'+(' '*49),curses.A_BOLD|curses.A_REVERSE)
            self.bar.addstr(5,3,' Game Over ',curses.A_BOLD|curses.A_REVERSE)
            self.bar.addstr(8,3,' Start another Game ',curses.A_BOLD|curses.A_REVERSE if self.gameOverChoice == 0 else curses.A_BOLD)
        self.bar.addstr(9,3,' Options ',curses.A_BOLD|curses.A_REVERSE if self.gameOverChoice == 1 else curses.A_BOLD)
        self.bar.addstr(10,3,' EXIT ',curses.A_BOLD|curses.A_REVERSE if self.gameOverChoice == 2 else curses.A_BOLD)
        self.bar.addstr(self.Y-2, 1,datetime.now().strftime(' %H:%M:%S '),curses.A_BOLD|curses.A_REVERSE)
        #self.bar.refresh()
        #curses.napms(1000)

    def exit(self):
        '''exit
        
        Reset curses settings, close the window generated in ``__init__``
        and quit the software
        
        :return: 
        :rtype: void
        '''
        curses.nocbreak()
        self.stdscr.nodelay(False)
        self.stdscr.keypad(0)
        curses.echo()
        curses.endwin()
        exit(0)
        
    def _setColor(self,action='plus'):
        '''_setColor
        
        Change interface color
        
        If action is equal 'plus' it will be set the next color, for example 
        if current color is 2, it will be setted 3.
        
        If action is equal to 'minus' it will be se the previus color, 
        for example if current color is 2, it will be setted 1.
        
        :param gameInit: Can be 'plus' or 'minus'
        :type gameInit: string
        
        :return: 
        :rtype: void
        '''
        if action == 'plus':
            self.defaultColorPair = self.defaultColorPair + 1
            if self.defaultColorPair >= 16:
                self.defaultColorPair = 0
            self.bar.bkgd(' ',curses.color_pair(self.defaultColorPair)) # Setting color of entire sub window
        elif action == 'minus':
            self.defaultColorPair = self.defaultColorPair - 1
            if self.defaultColorPair < 0:
                self.defaultColorPair = 15
            self.bar.bkgd(' ',curses.color_pair(self.defaultColorPair)) # Setting color of entire sub window
        
    def getKeyPressed(self,gameOver=False,gameInit=False):
        '''getKeyPressed
        
        Depending on where the user is and the pressed key are executed 
        various actions and/or is returned None or a string.
        
        Is returned None is the pressed key must not modify any game behavior 
        or state but only InitScreen() internal things. 
        
        Is returned a string that contain a directive that must be used by game.
        This string is returned when player's action must affect the main or 
        the ``Snake()`` class.
        
        With `game` I mean the main and the object instantiated by the class ``Snake()``
        
        :param gameOver: True when the player has lost or haven't yet started the game
        :type gameOver: boolean
        :param gameInit: True until the player start the first game
        :type gameInit: boolean
        
        :return: A string that represents the game state or None when it is not necessary return a game state
        :rtype: string
        '''
        userInput = self.stdscr.getch()
        if userInput == curses.KEY_RESIZE:
            self.bar.erase()
            terminalY, terminalX = self.stdscr.getmaxyx()
            if terminalY < self.Y or terminalX < self.X:
                self.errorSize = True
                return 'errorSize'
            else:
                self.errorSize = False
                self._initSubWin()
                return 'pause'
        if gameOver and self.currentPage != 'options':
            if userInput == curses.KEY_UP:
                self.gameOverChoice = 1 if self.gameOverChoice == 2 else 0
                self._printGameOverPage(gameInit)
                return None
            elif userInput == curses.KEY_DOWN:
                self.gameOverChoice = 1 if self.gameOverChoice == 0 else 2
                self._printGameOverPage(gameInit)
                return None
            elif userInput == curses.KEY_ENTER or userInput == 10 or userInput == 13:
                if self.gameOverChoice == 0: # Start a new game not yet implemented
                    return 'newGame'
                if self.gameOverChoice == 1:
                    if self.currentPage == 'home':
                        self.currentPage = 'options'
                    elif self.currentPage == 'options':
                        self.currentPage = 'home'
                    self.bar.clear()
                    self._initSubWin()
                    return 'pauseOn'
                elif self.gameOverChoice == 2:
                    self.exit()
                return None
        if self.currentPage == 'options':
            if userInput == curses.KEY_UP:
                if self.optionChoice == 0:
                    self.optionChoice = len(self.options)-1
                else:
                    self.optionChoice = self.optionChoice - 1
                self._printOptionPage()
                return None
            elif userInput == curses.KEY_DOWN:
                if self.optionChoice == len(self.options)-1:
                    self.optionChoice = 0
                else:
                    self.optionChoice = self.optionChoice + 1
                self._printOptionPage()
                return None
            if userInput == curses.KEY_RIGHT and self.optionChoice == 0:
                if gameInit or gameOver:
                    if self.currentYXPair < len(self.YXPairs)-1:
                        terminalY, terminalX = self.stdscr.getmaxyx()
                        self.currentYXPair = self.currentYXPair + 1
                        if self.YXPairs[self.currentYXPair][1] < terminalX and self.YXPairs[self.currentYXPair][0] < terminalY:
                            self._setInterfaceDimensions(self.YXPairs[self.currentYXPair][0],self.YXPairs[self.currentYXPair][1])
                            self.bar.clear()
                            self._initSubWin()
                            self._printOptionPage()
                        else:
                            self.currentYXPair = self.currentYXPair - 1
                return None
            if userInput == curses.KEY_LEFT and self.optionChoice == 0:
                if gameInit or gameOver:
                    if self.currentYXPair > 0:
                        self.currentYXPair = self.currentYXPair - 1
                        self._setInterfaceDimensions(self.YXPairs[self.currentYXPair][0],self.YXPairs[self.currentYXPair][1])
                    self.bar.clear()
                    self._initSubWin()
                    self._printOptionPage()
                return None
            elif userInput == curses.KEY_RIGHT and self.optionChoice == 1:
                if gameInit or gameOver:
                    if self.refreshTime < self._maxRefreshTime:
                        self._setRefreshTime(self.refreshTime+self._refreshStep)
                    self._printOptionPage()
                return None
            elif userInput == curses.KEY_LEFT and self.optionChoice == 1:
                if gameInit or gameOver:
                    if self.refreshTime > self._minRefreshTime:
                        self._setRefreshTime(self.refreshTime-self._refreshStep)
                    self._printOptionPage()
                return None
            elif userInput == curses.KEY_RIGHT and self.optionChoice == 2:
                self._setColor('plus')
                self._printOptionPage()
                return None
            elif userInput == curses.KEY_LEFT and self.optionChoice == 2:
                self._setColor('minus')
                self._printOptionPage()
                return None
            elif (userInput == curses.KEY_RIGHT or userInput == curses.KEY_LEFT) and self.optionChoice == 3:
                if gameInit or gameOver:
                    self.snakeAutocollisions = False if self.snakeAutocollisions else True
                    self._printOptionPage()
                    return 'toggleAutocollisions'
                return None
            elif (userInput == curses.KEY_RIGHT or userInput == curses.KEY_LEFT) and self.optionChoice == 4:
                self.enableColorTrill = False if self.enableColorTrill else True
                self._printOptionPage()
                if self.enableColorTrill:
                    self.colorTrill()
                return None
        if userInput == curses.KEY_RIGHT:
            return 'right'
        elif userInput == curses.KEY_LEFT:
            return 'left'
        elif userInput == curses.KEY_UP:
            return 'up'
        elif userInput == curses.KEY_DOWN:
            return 'down'
        elif userInput == ord('c'): # c char
            self._setColor('plus')
            return None
        elif userInput == ord('q') or userInput == 27: # q char or ESC
            self.exit()
        elif userInput == ord('o'): # o char
            if self.currentPage == 'home':
                self.currentPage = 'options'
                pauseStatus = 'pauseOn'
            elif self.currentPage == 'options':
                self.currentPage = 'home'
                pauseStatus = 'pauseOff'
            self.bar.clear()
            self._initSubWin()
            return pauseStatus
        elif userInput == ord('p'): # p char
            if self.currentPage == 'home':
                return 'pauseToggle'
            else:
                return None
        else:
            return None
            
    def colorTrill(self):
        '''colorTrill
        
        A color trill effect. Change screen foreground color 16 times 
        and then reset to initial color in an instant
        
        :return: 
        :rtype: void
        '''
        for i in range(16):
            curses.napms(6)
            self.bar.bkgd(' ',curses.color_pair(i)) # Setting color of entire sub window
            self.bar.refresh()
        self.bar.bkgd(' ',curses.color_pair(self.defaultColorPair)) # Setting color of entire sub window

    def refresh(self,snake,coin,score,gameOver=False,gameInit=True):
        '''refresh
        
        Refresh curses sub window every N milliseconds 
        Refresh time is ``self.refreshTime``
        
        :param snake: Multidimensional list that contain the coordinates of all snake body parts and the char that will represent it
        :type snake: list
        :param coin: Current coordinates and char that will represent the coin
        :type coin: list
        :param score: Current score
        :type score: int
        :param gameOver: True when the player has lost or haven't yet started the game
        :type gameOver: boolean
        :param gameInit: True until the player start the first game
        :type gameInit: boolean
        
        :return: 
        :rtype: void
        '''
        if not self.errorSize:
            #self.bar.clear()
            #self.bar.erase()
            if gameOver and self.currentPage != 'options':
                self._printGameOverPage(gameInit)
            elif self.currentPage == 'home':
                self._printHomePage(snake,coin)
            elif self.currentPage == 'options':
                self._printOptionPage()
            self.bar.addstr(self.Y-2, 1,datetime.now().strftime(' %H:%M:%S '),curses.A_BOLD|curses.A_REVERSE)
            self.bar.addstr(self.Y-2, 10,' SCORE '+str(score).zfill(8),curses.A_BOLD|curses.A_REVERSE)
            #self.bar.addstr(self.Y-2, 25,' '*43,curses.A_BOLD|curses.A_REVERSE)
            self.bar.addstr(self.Y-2, 25,' '*(self.YXPairs[self.currentYXPair][1]-27),curses.A_BOLD|curses.A_REVERSE)
            '''
            self.bar.addstr(self.Y-2, 10,' CPU: '+str(psutil.cpu_percent()) +'% ',curses.A_BOLD|curses.A_REVERSE)
            self.bar.addstr(self.Y-2, 21,' RAM: '+str(psutil.virtual_memory().percent) +'% ',curses.A_BOLD|curses.A_REVERSE)
            '''
            self.bar.refresh()
            curses.napms(self.refreshTime)
        else:
            curses.napms(1000)
            
    def setGameData(self,snake,coin,score,gameOver=False,gameInit=True):
        '''setGameData

        This will set ``InitScreen`` properties to avoid argument surround in some methods.
        
        .. todo::
            WIP !!! Write this method and modify ``self.refresh()`` ``self._printGameOverPage()``
            ``self._printHomePage()`` and ``self.getKeyPressed()`` to use 
            ``InitScreen`` properties instead method arguments

        :param snake: Multidimensional list that contain the coordinates of all snake body parts and the char that will represent it
        :type snake: list
        :param coin: Current coordinates and char that will represent the coin
        :type coin: list
        :param score: Current score
        :type score: int
        :param gameOver: True when the player has lost or haven't yet started the game
        :type gameOver: boolean
        :param gameInit: True until the player start the first game
        :type gameInit: boolean
        
        :return: 
        :rtype: void
        '''
        pass
