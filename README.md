# SnakeTerm
## Snake game for terminal written in Python and Curses


### The Game
Snake grows eating coins and die if collide with the walls or with its body

1 coin = 10 points
### How to play
* To start **SnakeTerm** run the following command:

	```
	python3 main.py
	```
* To start the game select `Start Game` and press enter
* Here are some useful keys that the player can use during the game:
	* `c` to change the interface color
	* `o` to toggle option page
	* `p` to pause game
	* `q` or ESC to quit the software
### Options
* You can go in option page through start game menù or pressing `o` character during the game
* When in option page, you can back to title screen or game pressing `o` character
* During the game some options are locked any the player can't modify them
* Navigate option menù with up/down arrow keys and change options with right/left arrow keys
* **Interface dimensions**  will set the X/Y size of the game interface
  * Locked during the game
  * Player can choice these sizes:
    * 70/20,100/20,130/20,160/20
    * 70/24,100/24,130/24,160/24
    * 70/30,100/30,130/30,160/30
  * If you can't see and use some sizes try to enlarge your terminal sizes
* **Interface refresh time** will set the speed and *difficulty*
  * Locked during the game
  * Player can choice the refresh time in milliseconds through 40 to 400 with 10 as step
  * Default refresh time is 100ms
* **Color** will set the entire interface color
  * Player can change this also during the game pressing `c` character
* **Snake autocollisions** change the snake behaviur
  * Locked during the game
    * ON is the default -> Snake die if collide with its body
    * OFF -> Snake can pass through its body
* **Color trill** will set a visual signal when Snake eat a coin.
### Requirements
* Python 3
* Developed and tested only on **Linux**. On windows you should also install the curses module because the windows version of Python doesn’t include the curses module
### Documentation
* There is an HTML documentation generated with `Sphinx 4.4.0`
* To read and navigate it clone the project and open `Docs/build/html/index.html` with a browser
* You can read **SnakeTerm** documentation also online, *maybe not even up to date*:
	* <a href="https://www.mangaraven.com/coolprojects/snaketerm/docs/" target="_blank">Click here to read online SnakeTerm documentation</a>
### Known problems
* If during the game Player change terminal sizes and these are less than **interface dimensions** values and then go to option page the software will crash and viceversa, in other similar cases the game simply do nothing or go in pause mode
### ToDo
* Implement `Screen.setGameData()`
* Implement HIGHSCORES. 
	* Highscores tables will be divided in this structure:
		* Snake Autocollisions ON/OFF as root of HIGHSCORE menu
		* Interface dimensions as child
		* Each interface dimensions have as children interface refresh times
* Upgrade `Sphinx` from `4.4.0` to `5.x`

